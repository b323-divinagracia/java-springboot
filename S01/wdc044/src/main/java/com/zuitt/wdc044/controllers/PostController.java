package com.zuitt.wdc044.controllers;

import java.util.List;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import com.zuitt.wdc044.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
//Enable cross origin requests via @CrossOrigin
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;


    //Create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }
    //get all post
    @GetMapping("/posts")
    public ResponseEntity<List<Post>> getAllPost(){
        List<Post> posts = (List<Post>) postService.getPost();
        if(posts.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }
}
    